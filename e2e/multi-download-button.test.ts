import { test, expect } from './fixtures';

test('multi download button has been added to containers with more than one attachment', async ({ page }) => {
  await page.goto(`https://${process.env.VITE_ZENDESK_DOMAIN}/agent/tickets/1`);

  for (const el of await page.locator('div.zdr-done').all()) { 
    await expect(el.locator('.zdr-dl-btn')).toHaveAttribute('title', 'Download all attachments for this comment');
  }
});

test('multi download button has not been added to containers with only one attachment', async ({ page }) => {
  await page.goto(`https://${process.env.VITE_ZENDESK_DOMAIN}/agent/tickets/1`);

  for (const el of await page.locator('div.attachment-group-container:has(div:only-child)').all()) { 
    await expect(el.locator('.zdr-dl-btn')).not.toBeVisible();
  }
});

test('multi download button icon is visible', async ({ page }) => {
  await page.goto(`https://${process.env.VITE_ZENDESK_DOMAIN}/agent/tickets/1`);

  for (const el of await page.locator('.zdr-dl-btn img').all()) { 
    await expect(el).toBeVisible();
  }
});