import { test as setup, expect } from '@playwright/test';

const authFile = './.playwright/auth/user.json';

setup('authenticate', async ({ page }) => {
  // Perform authentication steps.
  await page.goto('https://xxx/auth/v2/login/signin');
  await page.getByLabel('Email').fill('xxx');
  await page.getByLabel('Password', { exact: true }).fill('xxx');
  await page.getByRole('button', { name: 'Sign in' }).click();

  // Wait until the page receives the cookies.
  //
  // Sometimes login flow sets cookies in the process of several redirects.
  // Wait for the final URL to ensure that the cookies are actually set.
  await page.waitForURL('https://xxx/agent/dashboard');

  await page.context().storageState({ path: authFile });
});