export class DebugLogger {

  /**
   * Logs a debug message with a timestamp to the console if debug mode is enabled.
   * @param message - The debug message to log.
   */
  static async debugMessage(message: string): Promise<void> {
    try {
      const { debugMode } = await browser.storage.local.get(['debugMode']);
      if (debugMode) {
        this.logWithTimestamp(message);
      }
    } catch (error) {
      console.error('Error logging debug message:', error);
    }
  }

  private static logWithTimestamp(...messages: string[]): void {
    const timestamp = new Date().toISOString();
    console.info(`[${timestamp}]`, ...messages);
  }
}
