console.log('vitest.config.ts');
import { defineConfig } from 'vitest/config';
import { WxtVitest } from 'wxt/testing';

export default defineConfig({
  // Configure test behavior however you like
  test: {
    mockReset: true,
    restoreMocks: true,
    include: ['components/**/*.spec.ts', 'entrypoints/**/*.test.ts', 'entrypoints/**/*.spec.ts'],
    exclude: ['e2e/*'],
  },
  // This is the line that matters!
  plugins: [WxtVitest()],
});