<h1 align="center"> <img src="public/icon/32.png" alt="zd-dl-router icon" /> zd-dl-router</h1>
<p align="center">A cross browser extension that routes downloads from Zendesk into separate folders</p>
<h3 align="center">Made by <a href="https://gitlab.com/">GitLab Inc.</a></h3>
<hr />

## Features

- Cross Browser Support (Web-Extensions API)
- Routes tickets into separate folders based on ticket ID, ticket title and ticket organization
- Easily open previously downloaded files when viewing a ticket by clicking on the extension icon
- Download all attachments at once in the current ticket
- Download all attachments at once for each ticket comment

### Customising the download path

> **_NOTE:_**  The WebExtension API only allows saving into directories relative to the default download directory. Symlinks can be used to get around this limitation:
>
> Windows: `mklink /D C:\path\to\symlink D:\path\to\actual`
>
> Mac/Unix: `ln -s /path/to/actual /path/to/symlink`

The download path can be customized in the extension options:

![options](assets/images/options.png)

### Extension menu

The extension will show a context menu which will give you additional actions when viewing a Zendesk ticket:

![menu](assets/images/menu.png)

### Download all attachments for a comment

If a comment contains more than one attachment, the extension will show a button allowing you to download all attachments
for the respeftive comment:

![menu](assets/images/download-all-by-comment.png)

### Limitations

The Zendesk Download Router does contain specific limitations that will not be addressed:

- Attachments or images that are downloaded via the browser's context menu ("Save link as") will not be routed. For downloads to be routed,
  you must use the download buttons provided in the Zendesk ticket, or the extension drop down menu.
- Any Zendesk attachments that are downloaded via an email message from Zendesk will not be routed. This is because it is impossible to 
  determine which ticket the attachment belongs to without writing DOM parsers for each web email client.

## Screenshots

Does your download folder look like this?

<img src="assets/images/before.png" alt="before" style="zoom:50%;" />

Make it look like this:

<img src="assets/images/after.png" alt="after" style="zoom: 50%;" />

## Browser Support and install

| [![Chrome](https://raw.github.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png)](/) | [![Firefox](https://raw.github.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png)](/) | [![Opera](https://raw.github.com/alrra/browser-logos/master/src/opera/opera_48x48.png)](/) | [![Edge](https://raw.github.com/alrra/browser-logos/master/src/edge/edge_48x48.png)](/) | [![Brave](https://raw.github.com/alrra/browser-logos/master/src/brave/brave_48x48.png)](/) |
| --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Latest ✔ | Latest ✔ | Latest ✔ | Latest ✔ | Latest ✔ | Latest ✔ | Latest ✔
| [Install](https://chrome.google.com/webstore/detail/zendesk-download-router/pgfhacdbkdeppdjgighdeejjfneifkml) | [Install](https://addons.mozilla.org/en-GB/firefox/addon/zendesk-download-router/)<sup>1</sup> | [Install](https://addons.opera.com/en-gb/extensions/details/zendesk-download-router/)<sup>2</sup> | [Install](https://chrome.google.com/webstore/detail/zendesk-download-router/pgfhacdbkdeppdjgighdeejjfneifkml)<sup>3</sup> | [Install](https://chrome.google.com/webstore/detail/zendesk-download-router/pgfhacdbkdeppdjgighdeejjfneifkml) |

1. Firefox ESR and Firefox for Android are not supported.
1. Support for the Opera extension is discontinued and will not receive further updates.
1. Using the Chrome extension, and when **Allow extensions from other stores** is enabled. See [To add an extension to Microsoft Edge from the Chrome Web Store](https://support.microsoft.com/en-nz/help/4538971/microsoft-edge-add-or-remove-extensions) on the Microsoft Edge Support website for more information.

### Browser support disclaimer

This extension has been written to target Google Chrome first and foremost so it's always the first variant
of the extension that gets attention for bug fixes and the latest features.

Other browsers are not actively tested and bugs in other browsers may not be fixed by the extension author.

## Cleaning up the downloads

In order to avoid cluttering up your downloads folder,
regularly run [the `zd-dl-wiper`](https://gitlab.com/gitlab-com/support/toolbox/zd-dl-wiper).

Another option is using a tool like [Hazel](https://www.noodlesoft.com/) with rules like these:

![](https://gitlab.com/gitlab-com/support/toolbox/zd-dl-router/uploads/459a0790f77fde85f0f04cb6c78b4e3a/hazel-archive_old_ZDs.png)

![](https://gitlab.com/gitlab-com/support/toolbox/zd-dl-router/uploads/64c5720ba93ca5de02c16ec627ff5bb3/hazel-delete-old-ZD-archives.png)

## 🚀 Prerequsites for development

Ensure you have:

- [Node.js](https://nodejs.org) v18 or v20 installed
- [Yarn](https://yarnpkg.com) v4 installed

Then run the following:

- `yarn install` to install dependencies.
- `yarn dev:chrome` to start the development server for Chrome extension
- `yarn dev:firefox` to start the development server for Firefox addon
- `yarn test:vitest` to run Vitest unit tests
- `yarn test:e2e:playwright` to run Playwright end to end tests
- `yarn lint:eslint:check` to run a lint check on javascript source files
- `yarn lint:eslint:fix` to fix linter errors on javascript source files
- `yarn lint:prettier:check` to run a lint check on CSS and HTML source files
- `yarn lint:prettier:fix` to fix linter errors on CSS and HTML source files
- `yarn build:chrome` to build Chrome extension
- `yarn build:firefox` to build Firefox addon
- `yarn deploy:chrome` to deploy Chrome extension (`yarn run build:chrome` should be executed first)
- `yarn deploy:firefox` to deploy Firefox addon (`yarn run build:firefox` should be executed first)
- `yarn build` builds and packs extensions all at once to `extension/` directory

### Development

- `yarn install` to install dependencies.
- To watch file changes in developement

  - Chrome
    - `yarn dev:chrome`
  - Firefox
    - `yarn dev:firefox`

- **Load extension in browser**

  When development mode is executed, this will automatically open the respective browser in a new user profile
  with the extension preinstalled.

  If needed, the extension can be loaded in the browser manually:

  - ### Chrome

    - Go to the browser address bar and type `chrome://extensions`
    - Check the `Developer Mode` button to enable it.
    - Click on the `Load Unpacked Extension…` button.
    - Select your extension’s extracted directory (`.output/chrome-mv<VERSION>`).

  - ### Firefox

    - Load the Add-on via `about:debugging` as temporary Add-on.
    - Choose the `manifest.json` file in the extracted directory (`.output/firefox-mv<VERSION>`).

### Production

- `yarn build` builds the extension for all the browsers to `.output/<TARGET-BROWSER>-mv<VERSION>` directory respectively.

### Publishing to browser extension stores

> **_NOTE:_**  Follow [semantic versioning](https://semver.org/) when publishing a new version.

1. Increment the version in `package.json`, commit the changes and merge them with the `main` branch.
1. [Push a new tag](https://gitlab.com/gitlab-com/support/toolbox/zd-dl-router/-/tags/new) (using the new version as the tag name).
1. Merge the change into the `main` branch. A manual job to deploy the extensions for Chrome/Firefox will appear in the pipeline.
Start the `deploy` jobs manually for the extensions to be published to the respective store.

#### Environment variables

To deploy to the browser stores, you will need to generate a `.env.submit` file. This can be done by running `yarn dlx wxt submit init`
and following the prompts.

In addition, these environment variables need to be defined for deploy tasks to work:

- `CHROME_BADGE_API_URL`
- `FIREFOX_BADGE_API_URL`
- `PPAT`

#### Chrome refresh tokens

Refresh tokens are normally valid indefinitely unless they haven't been used in the last 6 months. In that case, the refresh token expires
and will prevent you from fetching new access tokens.

This can prevent you from publishing the Chrome extension, resulting in an error like this:

```shell

[info] Publishing Extension
[warn] Dry run, skipping submission
❯ Chrome Web Store
› [chrome] Checking ZIP files exist
› [chrome] Getting an access token
Reqeust: https://oauth2.googleapis.com/token
Response: {
  "_data": {
    "error": "invalid_grant",
    "error_description": "Bad Request"
  }
}
```

To resolve this, update the `CHROME_REFRESH_TOKEN` value in the `.env.submit` file with a new refresh token by following the
[Google docs](https://developer.chrome.com/docs/webstore/using-api#test-oauth).

## Troubleshooting

### `Error opening download folder: Error: No downloads were found for this attachment`

You may see a `Error opening download folder: Error: No downloads were found for this attachment` alert after clicking the `Open download folder`
while viewing a Zendesk ticket.

To determine which download folder to open in the operating system, the extension searches for a file that has been previously downloaded in
your browser's downloads history. The error can be shown if:

1. You have not previously downloaded any attachments on the current Zendesk ticket
1. The browser's download history has recently been cleared

### When I download a file, I get an HTML file instead containing `access-unauthenticated` in the body

The extension requires access to your Zendesk cookies in order to authenticate and allow downloading of Zendesk attachments.

If your Zendesk instance has [private attachments](https://support.zendesk.com/hc/en-us/articles/204265396-Enabling-attachments-in-tickets#topic_nrp_bnx_xdb) 
enabled, this can be an issue.

If you are using Firefox, ensure that first-party isolation is set to `false`. Browse to `about:config`, search for `privacy.firstparty.isolate`, and make sure it and all of the sub-keys are false.

For this same reason, if you are using [Firefox Multi-Account Containers](https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/), be sure that your Zendesk session is _not_ in a container, so that the zd-dl-router extension can access your Zendesk cookies.

### A download is not routed to where I expect

Review the [limitations](#limitations).

Please ensure you are using the latest version of your browser and extension.

If there are no [open issues](https://gitlab.com/gitlab-com/support/toolbox/zd-dl-router/-/issues) that match your problem,
please open a new issue with the following information:

- Your browser name and version
- The Zendesk ticket URL and attachment name you are trying to download
- Your routing configuration ie. where do you expect the download to end up?
- Any errors you might have seen in the **Console** tab of your browser's **Developer Tools**
- Reproduction steps. This is important so I can help you resolve the problem quickly.

#### For versions 3.5 and lower

Due to the way that the Zendesk agent application asynchronously loads data into the DOM, it can lead to race conditions
where the extension cannot extract the ticket information used to route downloads before an attachment is routed. When this
happens, any attachments won't be routed and they will be downloaded to the browser's default folder. If this happens,
refreshing the page and then attempting to download the attachment again will result in the extension routing the download
to the expected location.

## Show your support

Give a ⭐️ if this project helped you!

## Licence

Code released under the [MIT License](LICENSE).
