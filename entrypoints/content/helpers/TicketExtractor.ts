export class TicketExtractor {

  /**
   * Extracts the ticket ID from the current page URL.
   * Assumes that the ticket ID is the fourth segment in the URL path.
   * Example URL format: https://{subdomain}.zendesk.com/agent/tickets/{ticketId}
   * @returns {string} The ticket ID extracted from the URL.
   */
  public getTicketId() {
    return window.location.pathname.split('/')[3];
  }
  
  /**
   * Retrieves the title of the ticket from the page.
   * Assumes that the title can be found within a div[data-selected="true"] element with a specific data-test-id.
   * @returns {string | undefined} The ticket title if found, otherwise undefined.
   */
  public getTicketTitle() {
    return document.querySelector('div[data-selected="true"] div[data-test-id="header-tab-title"]')?.innerHTML.trim().replace(/(\r\n|\n|\r)/gm,'');
  }

  /**
   * Fetches the organization name associated with the ticket.
   * Assumes the organization's name is within a visible workspace element, avoiding any that are faded out or hidden.
   * @returns {string | undefined} The organization name if found, otherwise undefined.
   */
  public getTicketOrganization() { 
    return document.querySelector('div.workspace:not([style*="opacity: 0"]) span.organization-pill')?.innerHTML.trim().replace(/(\r\n|\n|\r)/gm, '');
  }

  /**
   * Extracts the ticket subdomain from the current page URL.
   * Assumes that the ticket subdomain is the first segment in the hostname.
   * Example hostname format: {subdomain}.zendesk.com
   * @returns {string} The ticket subdomain extracted from the URL.
   */
  public getTicketSubdomain() {
    return window.location.hostname.split('.')[0];
  }
  
  /**
   * Extracts the attachment count from the page.
   * @returns {number} The attachment count extracted from the page.
   */
  public getTicketAttachmentCount() {
    return document.querySelectorAll('div.workspace:not([style*="opacity: 0"]) a[data-test-id="attachment-thumbnail"]').length;
  }
}
