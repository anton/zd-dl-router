import { AttachmentHelper } from './AttachmentHelper';
import { ThumbnailResolver } from './ThumbnailResolver';

export class MultiDownloadButtonManager {
  private static readonly CONTAINER_PROCESSED_CLASS: string = 'zdr-attachment-group-processed';
  private static readonly ATTACHMENT_SELECTOR: string = 'div[data-test-id="attachment-group-container"], div[data-test-id=attachment-thumbnails-container]';

  /**
   * Initializes an observer to monitor for additions of attachment containers within the DOM.
   * When new attachment containers are identified, it evaluates whether a multi-download button should be added
   * based on the number of attachments within the container. This ensures that the multi-download functionality
   * is dynamically applied to relevant containers as they appear on the page.
   */
  public observe() {
    const observer = new MutationObserver((mutations) => {
      mutations.forEach((mutation) => {
        mutation.addedNodes.forEach((node) => {
          if (node.nodeType === Node.ELEMENT_NODE) {
            const element = node as Element;
            const attachmentsContainers = element.matches(MultiDownloadButtonManager.ATTACHMENT_SELECTOR) ? [element] : element.querySelectorAll(MultiDownloadButtonManager.ATTACHMENT_SELECTOR);
            attachmentsContainers.forEach(container => {
              // Immediately mark the container as processed to avoid re-processing in future mutations
              if (!container.classList.contains(MultiDownloadButtonManager.CONTAINER_PROCESSED_CLASS)) {
                container.classList.add(MultiDownloadButtonManager.CONTAINER_PROCESSED_CLASS);
            
                // Check and add button only if the container has more than one anchor element
                const attachmentCount = container.querySelectorAll('a, button').length;
                if (attachmentCount > 1) {
                  const button = this.createDownloadButton(attachmentCount);
                  container.prepend(button);
                }
              }
            });
          }
        });
      });
    });
    
    // Assuming the attachments are added to a known container or within the body
    observer.observe(document.documentElement, {
      childList: true,
      subtree: true
    });
  }

  /**
   * Attaches a click event listener at the document level to intercept clicks on the dynamically added download buttons.
   * This allows the application to handle the download of all attachments within a container programmatically,
   * utilizing the AttachmentHelper class to facilitate the download process.
   */
  public bindEventHandler() {
    document.addEventListener('click', function(event) {
      const target = event.target as HTMLAnchorElement | HTMLButtonElement | HTMLImageElement;
      
      const downloadAllButton = target.closest('.zdr-dl-btn-container');
      if (downloadAllButton) {
        event.preventDefault();
        const attachments = downloadAllButton.parentElement.querySelectorAll('a, button:not(.zdr-dl-btn)');

        // If buttons exist, we can resolve the direct src before downloading
        if(Array.from(attachments).some(node => node instanceof HTMLButtonElement)) {
          const buttons = Array.from(attachments).filter(node => node instanceof HTMLButtonElement);
          const ticketID = window.location.pathname.split('/')[3];
          const thumbnailResolver = new ThumbnailResolver(ticketID);
          
          buttons.forEach(button => {
            thumbnailResolver.resolveDirectSrc(button);
          });
        }

        attachments.forEach(attachment => {
          AttachmentHelper.downloadAttachment(attachment);
        });
        return;
      }
    });
  }

  /**
   * Constructs and returns a complete download button element including the button container,
   * the button itself, a count pill indicating the number of attachments, and an image icon.
   * @param {number} attachmentCount - The number of attachments within the container.
   * @returns {HTMLDivElement} The constructed button container element, ready to be inserted into the DOM.
   */
  private createDownloadButton(attachmentCount: number): HTMLDivElement {
    const buttonContainer = this.createButtonContainer();
    const button = this.setupDownloadButton(attachmentCount);
    const countPill = this.createCountPill(attachmentCount);
    const image = this.createButtonImage();

    button.appendChild(countPill);
    button.appendChild(image);
    buttonContainer.appendChild(button);
    return buttonContainer;
  }

  /**
   * Creates the container div for the download button, applying necessary styles and classes.
   * This container serves to group the button and any related elements (such as the count pill and image),
   * ensuring proper layout and styling.
   * @returns {HTMLDivElement} A div element configured as the button container.
   */
  private createButtonContainer(): HTMLDivElement {
    const container = document.createElement('div');
    container.className = 'zdr-dl-btn-container';
    return container;
  }

  /**
   * Prepares the actual download button, setting its class, title, and accessibility attributes.
   * This setup includes defining the button's role and labeling for accessibility, ensuring that users
   * understand its purpose and functionality.
   * @param {number} attachmentCount - The total number of attachments, used for accessibility labels.
   * @returns {HTMLButtonElement} The configured download button.
   */
  private setupDownloadButton(attachmentCount: number): HTMLButtonElement {
    const button = document.createElement('button');
    button.className = 'zdr-dl-btn';
    button.setAttribute('title', 'Download all attachments for this comment');
    button.setAttribute('aria-label', `Download all ${attachmentCount} attachments for this comment`);
    return button;
  }

  /**
   * Generates a visual indicator (count pill) to display the number of attachments available for download.
   * This pill adds a user-friendly component to the button, allowing users to quickly see how many attachments
   * are associated with the download action.
   * @param {number} attachmentCount - The number of attachments to display.
   * @returns {HTMLDivElement} A div element styled as a pill, containing the number of attachments.
   */
  private createCountPill(attachmentCount: number): HTMLDivElement {
    const countPill = document.createElement('div');
    countPill.className = 'zdr-dl-count';
    countPill.textContent = attachmentCount.toString();
    return countPill;
  }

  /**
   * Creates and configures the image element to be used as part of the download button,
   * sourcing the image from the extension's assets. This image visually represents the download action,
   * enhancing the button's appearance and user understanding.
   * @returns {HTMLImageElement} An image element configured for the download button.
   */
  private createButtonImage(): HTMLImageElement {
    const image = document.createElement('img');
    image.src = browser.runtime.getURL('/icon/128.png');
    image.style.cssText = 'width:60px;height:60px;';
    return image;
  }
}
