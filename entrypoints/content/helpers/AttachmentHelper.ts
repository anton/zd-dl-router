import { AlertHelper } from './AlertHelper';
import { DebugLogger } from '@/components/helpers/DebugLogger';
import { TicketExtractor } from './TicketExtractor';

export class AttachmentHelper {
  private static readonly ATTACHMENT_SELECTOR: string = 'div[data-test-id="attachment-group-container"]';
  private static readonly MODAL_ATTACHMENT_SELECTOR: string = 'div[data-test-id="attachment-modal"]';

  /**
   * Attaches a click event listener to the entire document which triggers on clicks within the specified attachment area.
   * This method specifically looks for click events on download buttons within the attachment group container,
   * preventing the default action to handle the download programmatically.
   */
  public static bindEventHandler() {
    document.addEventListener('click', function(event) {
      const target = event.target as Element;

      const attachmentLink = target.closest(AttachmentHelper.ATTACHMENT_SELECTOR + ' a') as HTMLAnchorElement;
      if (attachmentLink && !AttachmentHelper.isImageFile(attachmentLink.getAttribute('title'))) {
        event.preventDefault();
        AttachmentHelper.downloadAttachment(attachmentLink);
      }

      const modalAttachmentButton = target.closest(AttachmentHelper.MODAL_ATTACHMENT_SELECTOR + ' button[data-test-id="attachment-modal-download-button"]') as HTMLButtonElement;
      if (modalAttachmentButton) {
        event.preventDefault();
        event.stopPropagation();

        const image = target.closest(AttachmentHelper.MODAL_ATTACHMENT_SELECTOR).querySelector('img') as HTMLImageElement;
        AttachmentHelper.downloadAttachment(image);
      }
      
    }, true);
    DebugLogger.debugMessage('AttachmentHelper: binded event handler');
  }

  /**
   * Initiates the download process for an attachment.
   * It creates an object with details about the attachment and the ticket it belongs to,
   * then sends a message to the browser's runtime to handle the download action.
   *
   * This version supports both anchor elements (links) and image elements.
   * 
   * @param {HTMLAnchorElement | HTMLImageElement | HTMLButtonElement } attachment - The element linking to or displaying the attachment to be downloaded.
   */
  public static downloadAttachment(attachment: HTMLAnchorElement | HTMLImageElement | HTMLButtonElement) {
    if(!browser.runtime.id) {
      AlertHelper.flashMessage('The Zendesk Download Router extension was recently updated. Please refresh the page to continue using the extension.');
      return;
    }
    const ticketExtractor = new TicketExtractor();

    // Determine the URL based on the element type
    let url;
    if (attachment instanceof HTMLButtonElement) {
      url = attachment.hasAttribute('data-direct-src') ? attachment.getAttribute('data-direct-src') : null;
    }
    else {
      url = attachment instanceof HTMLAnchorElement ? attachment.href : attachment.src;
    }

    if(url === null) {
      AlertHelper.flashMessage('One or more image attachment URLs could not be resolved.');
      return;
    }

    const payload = {
      action: 'download-attachment',
      domain: window.location.hostname,
      url: url,
      id: ticketExtractor.getTicketId(),
      title: ticketExtractor.getTicketTitle(),
      organization: ticketExtractor.getTicketOrganization(),
      subdomain: ticketExtractor.getTicketSubdomain(),
    };

    browser.runtime.sendMessage(payload).then(()=> {
      DebugLogger.debugMessage('AttachmentHelper: Starting attachment download');
      DebugLogger.debugMessage(JSON.stringify(payload));
    });
  }
  
  public static downloadAllAttachments() {
    const attachments = document.querySelectorAll('div.workspace:not([style*="opacity: 0"]) [data-test-id="attachment-thumbnail"]');

    DebugLogger.debugMessage('AttachmentHelper: Downloading all attachments');

    attachments.forEach(attachment => {
      AttachmentHelper.downloadAttachment(attachment as HTMLAnchorElement);
    });
  }
  
  private static isImageFile(filename: string) {
    const imageExtensions = ['png', 'jpg', 'jpeg', 'gif', 'bmp', 'tiff', 'webp'];
    const extension = filename.split('.').pop().toLowerCase();

    return imageExtensions.includes(extension);
  }
}