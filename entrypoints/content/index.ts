import { MultiDownloadButtonManager } from '@/entrypoints/content/helpers/MultiDownloadButtonManager';
import { AttachmentHelper } from '@/entrypoints/content/helpers/AttachmentHelper';
import { AlertHelper } from './helpers/AlertHelper';
import './style.scss';

export default defineContentScript({
  // Set manifest options
  matches: ['*://*.zendesk.com/*', '*://*.apps.zdusercontent.com/*'],
  runAt: 'document_end',

  main(_ctx) {

    /**
     * Listens for messages from the background script and triggers ticket refresh on Zendesk ticket pages.
     * @param request - The message request from the background script.
     * @param _sender - The sender of the message.
     * @param _sendResponse - Function to send a response back.
     */
    browser.runtime.onMessage.addListener((request, _sender, _sendResponse) => {
      if (request.action === 'flashAlert') {
        console.log(request);
        AlertHelper.flashMessage(request.message);
      }
      if (request.action === 'download-all-attachments') {
        AttachmentHelper.downloadAllAttachments();
      }
    });

    AttachmentHelper.bindEventHandler();

    const multiDownloadButtonManager = new MultiDownloadButtonManager();
    multiDownloadButtonManager.observe();
    multiDownloadButtonManager.bindEventHandler();

    console.info('Content script initialised');
  },
});