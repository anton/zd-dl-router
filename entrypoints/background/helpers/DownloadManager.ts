import { DownloadPathResolver } from './DownloadPathResolver';

export class DownloadManager {

  /**
   * Opens the download folder based on the current active browser tab's URL.
   * This function queries the active tab in the current window, retrieves the URL of that tab,
   * and then utilizes `DownloadPathResolver` to open the folder where the download items related to the URL are stored.
   * The folder path is determined by extracting the last segment of the URL which usually represents the file or directory name.
   */
  public static openDownloadFolder = async () => {
    browser.tabs.query({active: true, currentWindow: true}).then((tabs) => {
      const currentTab = tabs[0];
      const currentUrl = currentTab.url;

      const downloadPathResolver = new DownloadPathResolver();
      downloadPathResolver.openDownloadFolder(currentUrl.split('/').pop());
    });
  };

  /**
   * Downloads an attachment using information provided in the `attachment` object.
   * This function first resolves the relative path where the attachment should be saved using a `DownloadPathResolver`.
   * It extracts the filename from the attachment's URL, cleans it of potential URL-encoded strings and characters not suitable for filenames,
   * and then initiates a download through the browser's download API.
   * Special handling includes domain substitution in the URL to handle specific authentication or domain-related errors.
   * 
   * @param attachment - The attachment object containing necessary details like id, title, organization, subdomain, url, and domain.
   */
  public static downloadAttachment = async (attachment: Attachment) => {
    const resolver = new DownloadPathResolver();
    const downloadPath = await resolver.determineRelativeDownloadPath(attachment.id, attachment.title, attachment.organization, attachment.subdomain);
    let filename = attachment.url
      .split('/')
      .pop() // Get last part of url (which is the filename component)
      .replace(/^(\?name=)/, '') // Replace '?name=' with empty string
      .replace(/^\./, '') // Replace '.' with empty string
      .replace(/\+/, ' '); // Replace '+' with space

    filename = decodeURI(filename);

    // Replace the domain with the Zendesk subdomain
    // This prevents the access-unauthenticated error when using the multi download button
    const url = new URL(attachment.url);
    url.hostname = attachment.domain;

    browser.downloads.download({
      filename: downloadPath + filename,
      url: url.toString(),
    });
  };
}