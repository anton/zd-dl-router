import { storage } from 'wxt/storage';

export class DownloadPathResolver {

  /**
   * Determines the relative download path based on specified variables and updates the storage accordingly.
   *
   * @param id - The unique identifier for the ticket, used to replace '%TICKET_ID%' in the download path.
   * @param title - The title of the ticket, used to replace '%TICKET_TITLE%' in the download path.
   * @param organization - The name of the organization associated with the ticket, used to replace '%TICKET_ORG%' in the download path. 
   * @param subdomain - The subdomain associated with the ticket, used to replace '%TICKET_SUBDOMAIN%' in the download path.
   * @returns A promise that resolves with the updated download path as a string.
   * @throws An error to the console if an error occurs during the path determination process.
   */
  public async determineRelativeDownloadPath(id: string, title: string, organization: string, subdomain: string): Promise<string> {
    try {
      const baseDownloadPath = await storage.getItem('local:baseDownloadPath') as string;
      if (!baseDownloadPath) {
        console.error('baseDownloadPath is not set in storage');
      }
      if(baseDownloadPath.startsWith('/')) {
        console.error('The download path cannot start with a forward slash. Check the extension settings.');
      }

      let transformPathToLowercase = await storage.getItem('local:transformPathToLowercase') as boolean;
      if (!transformPathToLowercase) {
        transformPathToLowercase = false;
      }

      let updatedDownloadPath = baseDownloadPath;

      // Replace variables in download path (and strip illegal characters)
      if (updatedDownloadPath.includes('%TICKET_ID%')) {
        updatedDownloadPath = updatedDownloadPath.replace('%TICKET_ID%', id);
      }

      if (updatedDownloadPath.includes('%TICKET_TITLE%')) {
        const ticketTitle = this.transformToSafeFolderName(title, transformPathToLowercase);
        updatedDownloadPath = updatedDownloadPath.replace('%TICKET_TITLE%', ticketTitle);
      }

      if (updatedDownloadPath.includes('%TICKET_ORG%')) {
        const ticketOrgName = this.transformToSafeFolderName(organization, transformPathToLowercase);
        updatedDownloadPath = updatedDownloadPath.replace('%TICKET_ORG%', ticketOrgName);
      }
      
      if(updatedDownloadPath.includes('%TICKET_SUBDOMAIN%')) {
        const ticketSubdomain = this.transformToSafeFolderName(subdomain, transformPathToLowercase);
        updatedDownloadPath = updatedDownloadPath.replace('%TICKET_SUBDOMAIN%', ticketSubdomain);
      }

      return Promise.resolve(updatedDownloadPath);
    } catch (error) {
      console.error('Error determining relative download path:', error);
    }
  };

  /**
   * Opens the download folder for the current ticket (if it exists)
   */
  public async openDownloadFolder(id: string) :Promise<void> {
    try {
      const results = await browser.downloads.search({ filenameRegex: id, exists: true, limit: 1 });

      // Only attempt to open the folder if we have results
      if (Array.isArray(results) && results.length) {
        browser.downloads.show(results[0].id);
      }
      else {
        throw new Error('No downloads were found for this attachment');
      };
    } catch (error) {
      console.error('Error opening download folder:', error);
    }
  };

  /**
   * Transform a string to a safe folder name by stripping any illegal characters
   * not allowed by the filesystem
   * @param {string} folderName The folder name to process
   * @param {boolean} toLower If the string should be converted to lowercase
   * @returns {string} The transformed safe folder name
   */
  private transformToSafeFolderName(folderName: string, toLower: boolean = false): string {
    if (!folderName) return '';

    let safeFolder = folderName;

    // Remove HTML entities in the string using a regex pattern
    safeFolder = safeFolder.replace(/&#[^\s]*;|&[^\s]*;/g, '');

    // Remove illegal characters using a regex pattern
    safeFolder = safeFolder.replace(/[|&;$%@".<>()+,/\\~:?]/g, '');

    // Replace consecutive spaces with a single space using a regex pattern
    safeFolder = safeFolder.replace(/\s{2,}/g, ' ');

    // Optionally convert to lowercase if desired
    if (toLower) safeFolder = safeFolder.toLowerCase();

    return safeFolder;
  };
}