/**
 * Intercepts and extends the functionality of the console.error method to capture error messages,
 * formats them, and then sends these formatted messages to be displayed as alerts in the browser tabs.
 * 
 * This class provides a way to globally intercept error logging and utilize browser-specific functionality
 * to communicate errors more visibly to the user through the web extension's capabilities.
 */
export class ConsoleErrorInterceptor {

  /**
   * Stores the original console.error function to allow for restoration later.
   */
  private originalConsoleError: (...data: unknown[]) => void;

  /**
   * Initializes the interceptor by saving the original console.error function and overriding it
   * with a custom implementation.
   */
  constructor() {
    this.originalConsoleError = console.error;
    this.overrideConsoleError();
  }

  /**
   * Overrides the global console.error function to intercept all error messages. Each message is
   * processed and sent to the active browser tab as an alert.
   * 
   * The override captures both simple string messages and Error objects, formatting them for display.
   * It maintains the original console.error functionality by logging the error to the console as well.
   */
  private overrideConsoleError(): void {
    console.error = (...args: unknown[]) => {
      this.originalConsoleError.apply(console, args);

      // Format all arguments into a single string message, handling Error objects to include detailed information.
      const fullMessage = args.map(arg => {
        if (arg instanceof Error) {
          // Format Error objects to include the message and stack trace.
          return `Error: ${arg.message}\nStack: ${arg.stack}`;
        }

        return String(arg);
      }).join(' ');

      // Send the formatted error message to be displayed as an alert in the active tab.
      this.flashAlert(fullMessage);
    };
  }

  /**
   * Sends the formatted error message to the active tab using the browser's messaging API.
   * This method is intended for internal use by the overrideConsoleError method.
   * 
   * @param {string} message - The formatted error message to send.
   */
  private flashAlert(message: string): void {
    browser.tabs.query({active: true, currentWindow: true}).then((tabs) => {
      if (tabs.length > 0) {
        browser.tabs.sendMessage(tabs[0].id, {action: 'flashAlert', message: message});
      }
    }).catch(error => {
      console.log('Error sending message to tab:', error);
    });
  }

  /**
   * Restores the original console.error function, removing the interception and custom behavior.
   * This method allows for dynamic control over the error interception feature.
   */
  public restoreOriginalConsoleError(): void {
    console.error = this.originalConsoleError;
  }
}
