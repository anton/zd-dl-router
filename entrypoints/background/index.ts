import { BrowserActionToggler } from './helpers/BrowserActionToggler';
import { ConsoleErrorInterceptor } from './helpers/ConsoleErrorInterceptor';
import { DownloadManager } from './helpers/DownloadManager';

export default defineBackground({
  // Set manifest options
  persistent: false,
  type: 'module',

  // Executed when background is loaded
  main() {

    /**
     * When the extension is initialised, set any default settings up for the first time
     */
    browser.runtime.onInstalled.addListener(() => {
      browser.storage.local
        .get({
          baseDownloadPath: 'zd-%TICKET_ID%/',
          transformPathToLowercase: false,
          debugMode: import.meta.env.DEV, // Enable debug mode by default in dev env
        })
        .then((s) => {
          // Set defaults in storage, in case we are loading the extension for the first time
          browser.storage.local.set({
            baseDownloadPath: s.baseDownloadPath,
            transformPathToLowercase: s.transformPathToLowercase,
            debugMode: s.debugMode,
          });

          // Remove retired settings from the sync storage since we use local storage now
          browser.storage.sync.clear();
        });
    });

    // Listener for receiving the current ticket title
    browser.runtime.onMessage.addListener((request, sender, _sendResponse) => {
      // Don't process if the sender tab is not active
      if (sender.tab &&!sender.tab.active) return Promise.resolve('ok');

      // Don't process autoloader messages
      if (request.type) return Promise.resolve('ok');

      if (request.action) {
        if (request.action === 'download-attachment') {
          return DownloadManager.downloadAttachment(request).then(() => 'ok').catch(error => {
            console.error('Failed to download attachment', error);
            return Promise.resolve('error');
          });
        }
        if (request.action === 'open-download-folder') {
          return DownloadManager.openDownloadFolder().then(() => 'ok').catch(error => {
            console.error('Failed to open download folder', error);
            return Promise.resolve('error');
          });
        }

        return Promise.resolve('ok');
      }

      return Promise.resolve('ok');
    });

    browser.tabs.onActivated.addListener((currentTab) => {
      browser.tabs.get(currentTab.tabId).then((tab) => {
        BrowserActionToggler.updateAction(tab);
      });
    });

    browser.windows.onFocusChanged.addListener((_windowId) => {
      browser.tabs.query({ active: true, currentWindow: true }).then((tabs) => {
        // Don't process if the sender window contains no tabs (developer console for example)
        if (tabs.length === 0) return;

        const tab = tabs[0];
        BrowserActionToggler.updateAction(tab);
      });
    });

    browser.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
      // Only process active tabs
      if (!tab.active) return;
      BrowserActionToggler.updateAction(tab);
    });

    // Override console.error to show errors in the UI
    new ConsoleErrorInterceptor();

    console.info('Background script initialised');
  },
});