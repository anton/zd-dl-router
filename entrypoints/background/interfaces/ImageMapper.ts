// eslint-disable-next-line @typescript-eslint/no-unused-vars
interface ImageMapper {
  src: string;
  thumb_src: string;
  file_name: string;
  date: string;
}