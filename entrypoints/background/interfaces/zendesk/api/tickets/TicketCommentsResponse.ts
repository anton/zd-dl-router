// eslint-disable-next-line @typescript-eslint/no-unused-vars
interface TicketCommentsResponse {
  comments: Comment[];
}

interface Comment {
  attachments: Attachment[];
  created_at: string;
}

interface Attachment {
  content_url: string;
  file_name: string;
  thumbnails: Thumbnail[];
}

interface Thumbnail {
  content_url: string;
}