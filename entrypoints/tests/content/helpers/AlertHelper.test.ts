// @vitest-environment happy-dom
import { describe, it, expect, beforeEach, afterEach } from 'vitest';
import { AlertHelper } from '@/entrypoints/content/helpers/AlertHelper';

describe('AlertHelper', () => {

  let alertElement: HTMLElement;

  beforeEach(() => {
    document.body.innerHTML = `
      <div id="alert">
        <p><b>Zendesk Alert:</b></p>
      </div>
    `;

    alertElement = document.getElementById('alert')!;
  });

  afterEach(() => {
    document.body.innerHTML = '';
  });

  it('should update alert HTML with message', () => {
    AlertHelper.flashMessage('Test message');
    
    expect(alertElement.innerHTML).toEqual('<p style="margin-right: 0px;">Test message</p>');
  });

  it('should add visible class to alert', () => {
    AlertHelper.flashMessage('Test message');

    expect(alertElement.classList.contains('visible')).toBeTruthy();
  });

  it('should remove visible class after delay', () => {
    AlertHelper.flashMessage('Test message');

    setTimeout(() => {
      expect(alertElement.classList.contains('visible')).toBeFalsy();
    }, 5100);
  });

  it('should reset alert HTML after delay', () => {
    AlertHelper.flashMessage('Test message');

    setTimeout(() => {
      expect(alertElement.innerHTML).toEqual('<p><b>Zendesk Alert:</b></p>');
    }, 5100);
  });

});