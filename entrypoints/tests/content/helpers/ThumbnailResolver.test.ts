// @vitest-environment happy-dom
import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { ThumbnailResolver } from '@/entrypoints/content/helpers/ThumbnailResolver';

describe('ThumbnailResolver', () => {
  let resolver: ThumbnailResolver;
  const mockXMLHttpRequest = {
    open: vi.fn(),
    send: vi.fn(),
    status: 200,
    responseText: JSON.stringify({
      comments: [
        {
          attachments: [
            {
              content_url: 'https://example.com/image1.jpg',
              thumbnails: [
                {
                  content_url: 'https://example.com/thumb1.jpg',
                },
              ],
              file_name: 'image1.jpg',
            },
            {
              content_url: 'https://example.com/image2.jpg',
              thumbnails: [],
              file_name: 'image2.jpg',
            },
          ],
          created_at: '2023-04-01T12:00:00Z',
        },
        {
          attachments: [],
          created_at: '2023-04-02T12:00:00Z',
        },
      ],
    }),
  };

  beforeEach(() => {
    vi.spyOn(window, 'XMLHttpRequest').mockImplementation(() => mockXMLHttpRequest as unknown as XMLHttpRequest);
    resolver = new ThumbnailResolver('123');
  });

  afterEach(() => {
    vi.restoreAllMocks();
  });

  it('should make a sync request to the correct URL', () => {
    expect(mockXMLHttpRequest.open).toHaveBeenCalledWith('GET', 'https://localhost:3000/api/v2/tickets/123/comments.json', false);
    expect(mockXMLHttpRequest.send).toHaveBeenCalledWith(null);
  });

  it('should process the response correctly', () => {
    expect(resolver.attachmentsData).toEqual([
      {
        src: 'https://example.com/image1.jpg',
        thumb_src: 'https://example.com/thumb1.jpg',
        file_name: 'image1.jpg',
        date: '2023-04-01T12:00:00',
      },
      {
        src: 'https://example.com/image2.jpg',
        thumb_src: null,
        file_name: 'image2.jpg',
        date: '2023-04-01T12:00:00',
      },
    ]);
  });

  it('should resolve direct src when thumbnail exists', () => {
    const attachment = document.createElement('button');
    const img = document.createElement('img');
    img.setAttribute('src', 'https://example.com/thumb1.jpg');
    attachment.appendChild(img);

    resolver.resolveDirectSrc(attachment);

    expect(attachment.getAttribute('data-direct-src')).toBe('https://example.com/image1.jpg');
  });

  it('should resolve direct src when thumbnail does not exist', () => {
    const attachment = document.createElement('button');
    attachment.setAttribute('title', 'image2.jpg');

    const article = document.createElement('article');
    const time = document.createElement('time');
    time.setAttribute('datetime', '2023-04-01T12:00:00.000Z');
    article.appendChild(time);

    const container = document.createElement('div');
    container.appendChild(article);
    article.appendChild(attachment);

    document.body.appendChild(container);

    resolver.resolveDirectSrc(attachment);

    expect(attachment.getAttribute('data-direct-src')).toBe('https://example.com/image2.jpg');

    document.body.removeChild(container);
  });
});