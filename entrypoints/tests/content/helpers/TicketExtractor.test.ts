// @vitest-environment happy-dom
import { describe, it, expect, beforeEach } from 'vitest';
import { TicketExtractor } from '@/entrypoints/content/helpers/TicketExtractor';

describe('TicketExtractor', () => {

  let ticketExtractor: TicketExtractor;

  beforeEach(() => {
    ticketExtractor = new TicketExtractor();
  });

  describe('getTicketId', () => {
    it('should extract the ticket ID from the URL', () => {
      delete window.location;
      window.location = new URL('https://example.zendesk.com/agent/tickets/12345');
      expect(ticketExtractor.getTicketId()).toBe('12345');
    });
  });

  describe('getTicketTitle', () => {
    it('should return the ticket title if present', () => {
      document.body.innerHTML = `
        <div data-selected="true">
          <div data-test-id="header-tab-title">Ticket Title</div>
        </div>
      `;
      expect(ticketExtractor.getTicketTitle()).toBe('Ticket Title');
    });

    it('should return undefined if the title element is not found', () => {
      document.body.innerHTML = '<div></div>';
      expect(ticketExtractor.getTicketTitle()).toBeUndefined();
    });
  });

  describe('getTicketOrganization', () => {
    it('should return the organization name if found', () => {
      document.body.innerHTML = `
        <div class="workspace" style="">
          <span class="organization-pill">Acme Inc.</span>
        </div>
      `;
      expect(ticketExtractor.getTicketOrganization()).toBe('Acme Inc.');
    });

    it('should return undefined if the organization element is not found', () => {
      document.body.innerHTML = '<div class="workspace" style="opacity: 0;"></div>';
      expect(ticketExtractor.getTicketOrganization()).toBeUndefined();
    });
  });

  describe('getTicketSubdomain', () => {
    it('should extract the subdomain from the URL', () => {
      delete window.location;
      window.location = new URL('https://example.zendesk.com/agent/tickets/12345');
      expect(ticketExtractor.getTicketSubdomain()).toBe('example');
    });
  });

  describe('getTicketAttachmentCount', () => {
    it('should return the count of attachments', () => {
      document.body.innerHTML = `
        <div class="workspace">
          <a data-test-id="attachment-thumbnail"></a>
          <a data-test-id="attachment-thumbnail"></a>
          <a data-test-id="attachment-thumbnail"></a>
        </div>
      `;
      expect(ticketExtractor.getTicketAttachmentCount()).toBe(3);
    });

    it('should return 0 if no attachments are found', () => {
      document.body.innerHTML = '<div class="workspace"></div>';
      expect(ticketExtractor.getTicketAttachmentCount()).toBe(0);
    });
  });
});