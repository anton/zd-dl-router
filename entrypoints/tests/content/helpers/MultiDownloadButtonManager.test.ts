// @vitest-environment happy-dom
import { describe, it, expect, beforeEach, vi } from 'vitest';
import { MultiDownloadButtonManager } from '@/entrypoints/content/helpers/MultiDownloadButtonManager';

describe('MultiDownloadButtonManager', () => {
  let manager: MultiDownloadButtonManager;  beforeEach(() => {
    manager = new MultiDownloadButtonManager();
  });

  describe('observe', () => {

    it('should create a MutationObserver', () => {
      const observeSpy = vi.spyOn(MutationObserver.prototype, 'observe');  
      manager.observe();
      expect(observeSpy).toHaveBeenCalled();
    });

    it('should observe the document element', () => {
      const observeSpy = vi.spyOn(MutationObserver.prototype, 'observe');
      manager.observe();
      expect(observeSpy).toHaveBeenCalledWith(document.documentElement, {
        childList: true, 
        subtree: true
      });
    });

  });

  describe('bindEventHandler', () => {

    it('should add click event listener to document', () => {
      const addEventListenerSpy = vi.spyOn(document, 'addEventListener');
      manager.bindEventHandler();    
      expect(addEventListenerSpy).toHaveBeenCalled();
    });

  });

  describe('createDownloadButton', () => {

    it('should create button container', () => {
      const button = manager.createDownloadButton(5);
      expect(button.className).toBe('zdr-dl-btn-container');
    });

    it('should create download button with attributes', () => {
      const button = manager.createDownloadButton(3);
      const downloadButton = button.querySelector('button');
      expect(downloadButton.className).toBe('zdr-dl-btn');
      expect(downloadButton.title).toBe('Download all attachments for this comment');
    });

  });
});