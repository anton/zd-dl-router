// @vitest-environment happy-dom
import { describe, it, expect, beforeEach, vi } from 'vitest';
import { AttachmentHelper } from '@/entrypoints/content/helpers/AttachmentHelper';
import { fakeBrowser } from 'wxt/testing';

describe('AttachmentHelper', () => {
  let document;

  beforeEach(() => {
    fakeBrowser.reset();

    document = globalThis.document;

    document.body.innerHTML = `
      <div class="workspace" style="opacity: 0">
        <div data-test-id="attachment-group-container">
          <a href="https://example.com/file.pdf" title="file.pdf">Download PDF</a>
          <a href="https://example.com/image1.jpg" title="image.jpg">Download Image</a>
        </div>
        <div data-test-id="attachment-modal">
          <button data-test-id="attachment-modal-download-button" data-direct-src="https://example.com/image2.jpg">Download Image</button>
          <img src="https://example.com/image3.jpg" alt="Image">
        </div>
      </div>
      
    `;
  });

  it('should bind event listeners correctly', () => {
    const addEventListenerMock = vi.fn();
    document.addEventListener = addEventListenerMock;
    AttachmentHelper.bindEventHandler();
    expect(addEventListenerMock).toHaveBeenCalled();
    expect(addEventListenerMock).toHaveBeenCalledWith('click', expect.any(Function), true);
  });
});