import { describe, it, expect, beforeEach } from 'vitest';
import background from '../../background';
import { fakeBrowser } from 'wxt/testing';
import { vi } from 'vitest';

fakeBrowser.commands = {
  // @ts-expect-error Mocks are only temporary
  onCommand: { 
    addListener: vi.fn()
  },
};
fakeBrowser.downloads = {
  // @ts-expect-error Mocks are only temporary
  onCreated: {
    addListener: vi.fn()
  },
};
fakeBrowser.downloads = {
  // @ts-expect-error Mocks are only temporary
  onDeterminingFilename: {
    addListener: vi.fn()
  },
};

describe('browser.runtime.onInstalled listener', () => {
  beforeEach(async () => {
    // Reset the in-memory state, including storage
    fakeBrowser.reset();

    background.main();
    await fakeBrowser.runtime.onInstalled.trigger({
      reason: 'install',
      temporary: false,
    });
  });

  it('should set the base download path on install', async () => {
    const actual = await storage.getItem('local:baseDownloadPath');
    expect(actual).toBe('zd-%TICKET_ID%/');
  });

  it('should set transformPathToLowercase to false on install', async () => {
    const actual = await storage.getItem('local:transformPathToLowercase');
    expect(actual).toBeFalsy();
  });

  // it('should set debugMode to false on install', async () => {
  //   const actual = await storage.getItem('local:debugMode');
  //   expect(actual).toBeFalsy();
  // });

  it('should clear any sync settings on install', async () => {
    const actual = await storage.getItem('sync:baseDownloadPath');
    expect(actual).toBeNull();
  });
});