import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { BrowserActionToggler } from '@/entrypoints/background/helpers/BrowserActionToggler';
import Browser from 'webextension-polyfill';

describe('BrowserActionToggler', () => {
  let tab: Browser.Tabs.Tab;

  beforeEach(() => {
    tab = {
      id: 1,
      url: 'https://example.zendesk.com/agent/tickets/123',
      index: 1,
      windowId: 1,
      highlighted: true,
      active: true,
      pinned: false,
      incognito: false
    };
    vi.spyOn(Browser.action, 'setIcon').mockImplementation(() => Promise.resolve());
    vi.spyOn(Browser.action, 'enable').mockImplementation(() => Promise.resolve());
    vi.spyOn(Browser.action, 'disable').mockImplementation(() => Promise.resolve());
  });

  afterEach(() => {
    vi.restoreAllMocks();
  });

  it('should enable action for agent URL', () => {
    BrowserActionToggler.updateAction(tab);

    expect(browser.action.setIcon).toHaveBeenCalledWith({
      tabId: 1,
      path: '/icon/128.png'  
    });
    expect(browser.action.enable).toHaveBeenCalledWith(1);
  });

  it('should disable action for non-agent URL', () => {
    tab.url = 'https://www.example.com';

    BrowserActionToggler.updateAction(tab);

    expect(browser.action.setIcon).toHaveBeenCalledWith({
      tabId: 1, 
      path: '/icon/bw128.png'
    });
    expect(browser.action.disable).toHaveBeenCalledWith(1);
  });
});
