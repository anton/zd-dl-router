import { defineConfig } from 'wxt';

const baseManifest = {
  name: 'Zendesk Download Router',
  description: 'Automatically routes Zendesk downloads into separate folders by ticket number',
  permissions: [
    'activeTab',
    'downloads',
    'storage',
    'tabs'
  ],
  web_accessible_resources: [{
    resources: ['/icon/128.png'],
    matches: ['*://*.zendesk.com/*', '*://*.apps.zdusercontent.com/*'],
  }],
};

const firefoxSpecificSettings = {
  browser_specific_settings: {
    gecko: {
      id: '{fa5f4648-bc4c-4986-ba5c-4a1b52a2eb42}'
    }
  }
};

// See https://wxt.dev/api/config.html
export default defineConfig({
  manifest: ({ browser }) => ({
    ...baseManifest,
    ...(browser === 'firefox' ? firefoxSpecificSettings : {}),
  }),
  runner: {
    // Don't load the browser when executing yarn dev
    // This prevents a new browser profile from being loaded
    disabled: true
  }
});
